#include "Objekt.h"
#include "PohyblivyObjekt.h"
#include "StatickyObjekt.h"
#include "Monstrum.h"
#include "Hra.h"
#include <Random>
#include<iostream>
using namespace std;

int main() {

	srand(time_t(nullptr));
	
	


	Hra hra = Hra();




	Objekt* o = new Objekt(1);
	o->setX(1);
	o->setY(1);
	hra.vlozObjekt(o);



	StatickyObjekt* so = new StatickyObjekt(3, TypPrekazky::MalaRostlina);
	so->setX(3);
	so->setY(3);
	hra.vlozObjekt(so);

	PohyblivyObjekt* po = new PohyblivyObjekt(2, 1);
	po->setX(2);
	po->setY(2);
	hra.vlozObjekt(po);

	Monstrum* mo = new Monstrum(1, 15, 20, 5);
	mo->setX(4);
	mo->setY(4);
	hra.vlozObjekt(mo);


	cout << hra.najdiIdStatickychObjektu(1, 5, 1, 5) << endl;
	cout << hra.najdiPohybliveObjektyVOblasti(1, 1, 20) << endl;
	cout << hra.najdiPohybliveObjektyVOblasti(1, 1, 20, 0, 8) << endl;




}