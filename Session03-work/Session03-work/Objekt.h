#ifndef OBJEKT_H
#define OBJEKT_H
class Objekt
{

private: 
	int id;
	double x;
	double y;
public:
	Objekt(int id);
	virtual ~Objekt();
	void setX(double x);
	void setY(double y);
	double getX()const;
	double getY()const;
	int getId()const;

};


#endif // OBJEKT_H

