#ifndef MONSTRUM_H
#define MONSTRUM_H
#include "PohyblivyObjekt.h"
class Monstrum : public PohyblivyObjekt{
private:
	int hp;
	int maxHp;
public:
	Monstrum(int id, int hp, int maxHp, double uhel);
	int getHp()const;
	int getMaxHp()const;
	void setHp(int hp);
	void setMaxHp(int maxHp);

};


#endif // MONSTRUM_H
