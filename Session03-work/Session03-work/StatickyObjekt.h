#ifndef STATICKYOBJEKT_H
#define STATICKYOBJEKT_H
#include "Objekt.h"
enum class TypPrekazky { Skala, MalaRostlina, VelkaRostlina };

class StatickyObjekt : public Objekt
{
private:
	TypPrekazky typPrekazky;
public:
	StatickyObjekt(int id, TypPrekazky typPrekazky);
	TypPrekazky getTypPrekazky()const;



};

#endif //STATICKYOBJEKT_H

