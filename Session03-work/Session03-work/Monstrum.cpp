#ifndef _MONSTRUM_H
#define _MONSTRUM_H


#include "Monstrum.h"



Monstrum::Monstrum(int id, int hp, int maxHp, double uhel) : PohyblivyObjekt(id, uhel)
{
	this->hp = hp;
	this->maxHp = maxHp;
}

int Monstrum::getHp()const
{
	return hp;
}

int Monstrum::getMaxHp()const
{
	return maxHp;
}

void Monstrum::setHp(int hp)
{
	this->hp = hp;
}

void Monstrum::setMaxHp(int maxHp)
{
	this->maxHp = maxHp;
}
#endif // !_MONSTRUM_H
