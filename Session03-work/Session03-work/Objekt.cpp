#include "Objekt.h"

Objekt::Objekt(int id)
{
	this->id = id;
}

Objekt::~Objekt()
{
	
}

void Objekt::setX(double x)
{
	this->x = x;
}

void Objekt::setY(double y)
{
	this->y = y;
}

double Objekt::getX()const
{
	return x;
}

double Objekt::getY()const
{
	return y;
}

int Objekt::getId()const
{
	return id;
}
