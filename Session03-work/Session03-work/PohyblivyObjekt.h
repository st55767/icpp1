#ifndef POHYBLIVYOBJEKT_H
#define POHYBLIVYOBJEKT_H

#include "Objekt.h"
#include <stdexcept>
#include "math.h"
#define M_PI 3.14159265358979323846  /* pi */

class PohyblivyObjekt : public Objekt
{
private:
	double uhelNatoceni; 
public:
	PohyblivyObjekt(int id, double uhelNatoceni);
	void setUhel(double uhel);
	double getUhel()const;

};



#endif // !POHYBLIVYOBJEKT_H


