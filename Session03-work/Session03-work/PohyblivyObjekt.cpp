

#include "PohyblivyObjekt.h"



PohyblivyObjekt::PohyblivyObjekt(int id, double uhelNatoceni) : Objekt(id)
{
	setUhel(uhelNatoceni);
}

void PohyblivyObjekt::setUhel(double uhel) 
{
	if (uhel > 0 && uhel < 2 * M_PI) {
		this->uhelNatoceni = uhel;

	}
	else
	{
		throw std::out_of_range("Neplatny uhel");
	}
}

double PohyblivyObjekt::getUhel()const
{

	return uhelNatoceni;
}



