#ifndef HRA_H
#define HRA_H
#define POCET_OBJEKTU 20
#include"Objekt.h";
#include"PohyblivyObjekt.h";


class Hra
{
private:
	Objekt** objekty;
	int idNaPoli= 0;
public:
	Hra();
	~Hra();
	void vlozObjekt(Objekt* o);
	int* najdiIdStatickychObjektu(double xmin, double xmax, double ymin, double ymax)const;
	PohyblivyObjekt** najdiPohybliveObjektyVOblasti(double x, double y, double r)const;
	PohyblivyObjekt** najdiPohybliveObjektyVOblasti(double x, double y, double r, double umin, double umax)const;

};

#endif // !HRA_H

