#include "Hra.h"
#include <math.h>
#include "StatickyObjekt.h"

Hra::Hra()
{

	objekty = new Objekt * [POCET_OBJEKTU];
	//relokace pole neni resena
}

Hra::~Hra()
{
	for (int i = 0; i < POCET_OBJEKTU; i++)
	{
		delete objekty[i];
	}
	delete[] objekty;
}

void Hra::vlozObjekt(Objekt* o)
{
	objekty[idNaPoli] = o;
	idNaPoli++;

}

int* Hra::najdiIdStatickychObjektu(double xmin, double xmax, double ymin, double ymax)const
{
	int pocet = 0;
	for (size_t i = 0; i < idNaPoli; i++)
	{
		Objekt* o = objekty[i];
		StatickyObjekt* so = dynamic_cast<StatickyObjekt*>(o);
		if (so = nullptr)
		{
			continue;
		}
		else if (so->getX() > xmin&& so->getX() < xmax&& so->getY() > ymin&& so->getY() < ymax)
		{
			pocet++;
		}
	}
	int aktualniIndex = 0;
	int* nalezeneObjekty = new int[pocet];
	for (size_t i = 0; i < idNaPoli; i++)
	{
		Objekt* o = objekty[i];
		StatickyObjekt* so = dynamic_cast<StatickyObjekt*>(o);
		if (so == nullptr)
		{
			continue;

		}
		if (so->getX() > xmin&& so->getX() < xmax&& so->getY() > ymin&& so->getY() < ymax)
		{
			nalezeneObjekty[aktualniIndex] = o->getId();
			aktualniIndex++;
		}
	}


	return nalezeneObjekty;
}

PohyblivyObjekt** Hra::najdiPohybliveObjektyVOblasti(double x, double y, double r)const
{
	int pocet = 0;
	for (size_t i = 0; i < idNaPoli; i++)
	{
		Objekt* o = objekty[i];
		PohyblivyObjekt* po = dynamic_cast<PohyblivyObjekt*>(o);
		if (po = nullptr)
		{
			continue;
		}
		if ((pow(po->getX() - x, 2) + pow(po->getY() - y, 2) < pow(r, 2)))
		{
			pocet++;
		}
	}
	int aktualniIndex = 0;
	PohyblivyObjekt** nalezeneObjekty = new PohyblivyObjekt * [pocet];
	for (size_t i = 0; i < idNaPoli; i++)
	{
		Objekt* o = objekty[i];
		PohyblivyObjekt* po = dynamic_cast<PohyblivyObjekt*>(o);
		if (po == nullptr)
		{
			continue;

		}
		if ((pow(po->getX() - x, 2) + pow(po->getY() - y, 2) < pow(r, 2)))
		{
			nalezeneObjekty[aktualniIndex] = po;
			aktualniIndex++;
		}
	}


	return nalezeneObjekty;
}

PohyblivyObjekt** Hra::najdiPohybliveObjektyVOblasti(double x, double y, double r, double umin, double umax)const
{
	int pocet = 0;
	for (size_t i = 0; i < idNaPoli; i++)
	{
		Objekt* o = objekty[i];
		PohyblivyObjekt* po = dynamic_cast<PohyblivyObjekt*>(o);
		if (po = nullptr)
		{
			continue;
		}
		if ((pow(po->getX() - x, 2) + pow(po->getY() - y, 2) < pow(r, 2)) && po->getUhel() > umin&& po->getUhel() < umax)
		{
			pocet++;
		}
	}
	int aktualniIndex = 0;
	PohyblivyObjekt** nalezeneObjekty = new PohyblivyObjekt * [pocet];
	for (size_t i = 0; i < idNaPoli; i++)
	{
		Objekt* o = objekty[i];
		PohyblivyObjekt* po = dynamic_cast<PohyblivyObjekt*>(o);
		if (po == nullptr)
		{
			continue;

		}
		if ((pow(po->getX() - x, 2) + pow(po->getY() - y, 2) < pow(r, 2)) && po->getUhel() > umin&& po->getUhel() < umax)
		{
			nalezeneObjekty[aktualniIndex] = po;
			aktualniIndex++;
		}
	}


	return nalezeneObjekty;
}
