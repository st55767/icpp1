#ifndef MATICE_H
#define MATICE_H
#include <iostream>
#include <exception>

template<typename T>
class Matice {
public:
	Matice(int aRadky, int aSloupce);
	Matice(const Matice& matice);
	~Matice();
	void set(const int& aRadek, const int& aSloupec, T hodnota);
	void set(T* pole);
	T& get(const int& aRadek, const int& aSloupec);
	const T& get(const int& aRadek, const int& aSloupec)const;
	template<typename R>
	Matice<R> pretypuj() const;
	Matice<T> transpozice()const;
	Matice<T> soucin(const Matice<T>& m)const;
	Matice<T> soucin(T skalar)const;
	Matice<T> soucet(const Matice<T>& m)const;
	Matice<T> soucet(T skalar)const;
	bool jeShodna(const Matice<T>& m)const;
	void vypis();

private:
	T** hodnoty;
	int radky;
	int sloupce;
};


#endif // MATICE_H



template<typename T>
inline Matice<T>::Matice(int aRadky, int aSloupce)
{
	radky = aRadky;
	sloupce = aSloupce;
	hodnoty = new T * [aRadky];
	for (int i = 0; i < aRadky; i++)
	{
		hodnoty[i] = new T[aSloupce];
	}
}

template<typename T>
inline Matice<T>::Matice(const Matice& matice)
{
	radky = matice.radky;
	sloupce = matice.sloupce;
	hodnoty = new T * [radky];
	for (int i = 0; i < radky; i++)
	{
		hodnoty[i] = new T[sloupce];
	}
}

template<typename T>
inline Matice<T>::~Matice()
{
	for (int i = 0; i < radky; i++)
	{
		delete[] hodnoty[i];
	}
	delete[] hodnoty;
}

template<typename T>
inline void Matice<T>::set(const int& aRadek, const int& aSloupec, T hodnota)
{
	if (radky <= aRadek || sloupce <= aSloupec)
	{
		throw std::logic_error("Neplatny index");
	}
	hodnoty[aRadek][aSloupec] = hodnota;
}

template<typename T>
inline void Matice<T>::set(T* pole)
{
	int dalsi = 0;
	for (int i = 0; i < radky; i++)
	{
		for (int j = 0; j < sloupce; j++)
		{
			hodnoty[i][j] = pole[dalsi++];
		}

	}
}

template<typename T>
inline T& Matice<T>::get(const int& aRadek, const int& aSloupec)
{
	if (radky <= aRadek || sloupce <= aSloupec)
	{
		throw std::logic_error("Neplatny rozmer matice");
	}
	return hodnoty[aRadek][aSloupec];
}

template<typename T>
inline const T& Matice<T>::get(const int& aRadek, const int& aSloupec) const
{
	if (radky <= aRadek || sloupce <= aSloupec)
	{
		throw std::logic_error("Neplatny rozmer matice");
	}
	return hodnoty[aRadek, aSloupec];
}

template<typename T>
inline Matice<T> Matice<T>::transpozice() const
{
	Matice<T> m = Matice(radky, sloupce);
	for (int i = 0; i < radky; i++)
	{
		for (int j = 0; j < sloupce; j++)
		{
			m.hodnoty[j][i] = hodnoty[i][j];
		}
	}
	return m;
}

template<typename T>
inline Matice<T> Matice<T>::soucin(const Matice<T>& m) const
{
	if (sloupce != m.radky) {
		throw std::logic_error("Nespravna velikost pro nasobeni matic");
	}
	Matice<T>ma = Matice(radky, sloupce);
	for (int i = 0; i < radky; i++)
	{
		for (int j = 0; j < sloupce; j++)
		{
			ma.hodnoty[i][j] = 0;
		}
	}
	for (int i = 0; i < radky; i++)
	{
		for (int j = 0; j < ma.sloupce; j++)
		{
			for (int k = 0; k < sloupce; k++)
			{
				ma.hodnoty[i][j] += hodnoty[i][k] * m.hodnoty[k][j];

			}
		}
	}
	return ma;
}

template<typename T>
inline Matice<T> Matice<T>::soucin(T skalar) const
{
	Matice<T> m = Matice(radky, sloupce);
	for (int i = 0; i < radky; i++)
	{
		for (int j = 0; j < sloupce; j++)
		{
			m.hodnoty[i][j] = m.hodnoty[i][j] * skalar;
		}
	}
	return m;
}

template<typename T>
inline Matice<T> Matice<T>::soucet(const Matice<T>& m) const
{
	if (radky <= m.radky || sloupce <= m.sloupce)
	{
		throw std::logic_error("Neplatny rozmer matice");
	}
	Matice<T> ma = Matice(radky, sloupce);
	for (int i = 0; i < radky; i++)
	{
		for (int j = 0; j < sloupce; j++)
		{
			ma[i][j] = hodnoty[i][j] + m.hodnoty[i][j];
		}
	}
	return ma;
}

template<typename T>
inline Matice<T> Matice<T>::soucet(T skalar) const
{
	Matice<T> m = Matice(radky, sloupce);
	for (int i = 0; i < radky; i++)
	{
		for (int j = 0; j < sloupce; j++)
		{
			m[i][j] = hodnoty[i][j] + skalar;

		}
	}
	return m;
}

template<typename T>
inline bool Matice<T>::jeShodna(const Matice<T>& m) const
{
	if (radky <= m.radky || sloupce <= m.sloupce)
	{
		//throw std::logic_error("Neplatny rozmer matice"); neni vhodne je lepsi return false
		return false;
	}
	for (int i = 0; i < radky; i++)
	{
		for (int j = 0; j < sloupce; j++)
		{
			if (hodnoty[i][j] != m.hodnoty[i][j])
			{
				return false;

			}
		}
	}
	return true;
}

template<typename T>
inline void Matice<T>::vypis()
{
	for (int i = 0; i < radky; i++)
	{
		for (int j = 0; j < sloupce; j++)
		{
			std::cout << hodnoty[i][j] << " ";
		}
		std::cout << std::endl;
	}
}

template<typename T>
template<typename R>
inline Matice<R> Matice<T>::pretypuj() const
{
	Matice<R> matice = Matice<R>(radky, sloupce);
	for (int i = 0; i < radky; i++)
	{
		for (int j = 0; j < sloupce; j++)
		{
			matice.set(i, j, static_cast<R>(hodnoty[i][j]));
		}
	}
	return matice;
}
