#include "Matice.h"
int main()
{
	Matice<int> m{ 3,3 };
	int jednodpole[] = { 0,1,2,3,4,5,6,7,8 };
	m.set(jednodpole);
	m.vypis();
	Matice<int> mt = m.transpozice();
	mt.vypis();
	Matice<int> mmt = m.soucin(mt);
	Matice<double> mmt_d = mmt.pretypuj<double>();
	Matice<double> n_d{ 3,3 };
	double jednodpole_d[] = { 4.5,5,0,2,-0.5,7,1.5,9,6 };
	n_d.set(jednodpole_d);
	Matice<double> mmtn_d = mmt_d.soucin(n_d);
	Matice<int> r = mmtn_d.pretypuj<int>();
	Matice<int> t{ 3,3 };
	int tpole[] = { 85,225,236,292,819,866,499,1413,1496 };
	t.set(tpole);
	r.vypis();
	t.vypis();
	std::cout << "r==t ? " << (r.jeShodna(t) ? "true" : "false") << std::endl;
	return 0;
};