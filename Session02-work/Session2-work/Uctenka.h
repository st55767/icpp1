#ifndef UCTENKA_H
#define UCTENKA_H


class Uctenka 
{
private:
	int cisloUctenky;
	double castka;
	double dhp;

public:
	int getCisloUctenky();
	double getCastka();
	double getDPH();
	void setCisloUctenky(int cisloUctenky);
	void setCastka(double castka);
	void setDPH(double dph);

};

#endif