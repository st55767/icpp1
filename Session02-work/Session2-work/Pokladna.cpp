#include "Pokladna.h"

#define POCET_UCTENEK 10
#define POCITAC_ID 1000
int Pokladna::citacID = POCITAC_ID;

Pokladna::Pokladna()
{
	this->uctenky = new Uctenka[POCET_UCTENEK];
}

Pokladna::~Pokladna()
{
	delete[] uctenky;
}

Uctenka& Pokladna::vystavUctenku(double castka, double dph)
{
	Uctenka uctenka = Uctenka();
	uctenka.setCisloUctenky(citacID++);
	uctenka.setCastka(castka);
	uctenka.setCastka(dph);

	this->uctenky[pocetVydanychUctenek++] = uctenka;
	citacID++;
	return uctenka;

}

Uctenka& Pokladna::dejUctenku(int cislo)
{
	if (cislo>= pocetVydanychUctenek)
	{
		return uctenky[0];
	}
	return uctenky[cislo];
}

double Pokladna::dejCastku() const
{
	double celkovaCastka = 0;
	for (int i = 0; i < pocetVydanychUctenek; i++)
	{
		celkovaCastka += uctenky[i].getCastka();
	}
	return celkovaCastka;
}

double Pokladna::dejCastkuVcDph() const
{
	double castkaVcDph = 0;
	for (int i = 0; i < pocetVydanychUctenek; i++)
	{
		castkaVcDph += (uctenky[i].getCastka() * (1 + uctenky[i].getDPH()));

	}
	return castkaVcDph;
}
