#ifndef POKLADNA_H
#define POKLADNA_H
#include "Uctenka.h"



class Pokladna {
private:

	static int citacID;
	Uctenka* uctenky;
	int pocetVydanychUctenek = 0;

public:

	Pokladna();

	~Pokladna();

	

	Uctenka& vystavUctenku(double, double);

	Uctenka& dejUctenku(int);

	double dejCastku() const;

	double dejCastkuVcDph() const;
};



#endif POKLADNA_H
