#include <iostream>
#include "Uctenka.h"
#include "Pokladna.h"

using namespace std;

int main() {

	Pokladna pokladna1{};

	pokladna1.vystavUctenku(123.0, 0.1);
	pokladna1.vystavUctenku(456.0, 0.2);
	pokladna1.vystavUctenku(789.0, 0.3);
	pokladna1.vystavUctenku(500.0, 0.4);

	Uctenka& u = pokladna1.dejUctenku(1000);

	int castka = pokladna1.dejCastku();

	int castkaVcDph = pokladna1.dejCastkuVcDph();

	cout << u.getCastka() << "  " << u.getCisloUctenky() << endl;

	cout << "castka: " << castka << endl;

	cout << "castka vcetne dph: " << castkaVcDph << endl;

	system("pause");
	return 0;
}
