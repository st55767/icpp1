#include <fstream>
#include <iostream>
#include <string>

using namespace std;
void uloz() {

#define z(x) binarniSoubor.write((char*)&(x), sizeof(x))

	ofstream binarniSoubor{ "soubor.dat", ios_base::out | ios_base::binary };

	double dcislo = 3.14529;


	z(dcislo); // stejne pro int a char



	int  poleOsklivychIntegeru[] = { 1,2,3,4,5,6,7,8,9,10 };
	z(poleOsklivychIntegeru);


	binarniSoubor.close();
	// lze cist pomoci online editoru Hexed.it
}

void nacti() {
	ifstream binarniSoubor{ "soubor.dat", ios_base::in | ios_base::binary };

	int cislo = 0;
	binarniSoubor.read((char*)&cislo, sizeof(cislo));

	char* znaky = new char[8];
	binarniSoubor.read(znaky, 8); // nelze sizeof(char*) protoze dynamicky alokovane pole
	delete[] znaky;

	int* pole = new int[10];
	binarniSoubor.read((char*)pole, 10 * sizeof(int));
	for (int i = 0; i < 10; i++)
	{
		cout << pole[i] << ",";
		cout << endl;
	}
	delete[] pole;


}
struct Barva {
	int r, b, g;
	
};
void ulozStrukturu() {
	ofstream f{ "soubor.dat", ios_base::out | ios_base::binary };

	Barva oskliva{ 120,250,30 };
	f.write((char*)&oskliva, sizeof oskliva); // jistejsi ale pracnejsi reseni je zapisovat jednotlive atributy samostatne
												// vytvari se bordel v souboru z duvodu zarovnavani pameti kompilatorem
												// spolehlivejsi je po jednotlivych atributech

	f.close();

}

int main() {

	return 0;
}