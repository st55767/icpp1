#ifndef OSOBA_H
#define OSOBA_H

#include <string>
#include <iostream>
#include <fstream>

struct Datum {
	int _den;
	int _mesic;
	int _rok;
};

struct Adresa {
	std::string _ulice;
	std::string _mesto;
	int _psc;
};

struct Osoba {
	std::string _jmeno;
	std::string _prijmeni;
	Adresa _trvaleBydliste;
	Datum _datumNarozeni;
};

std::ostream& operator<<(std::ostream& output, const Osoba& o);
std::istream& operator>>(std::istream& input, Osoba& o);
std::ostream& operator<<(std::ostream& output, const Adresa& a);
std::istream& operator>>(std::istream& input, Adresa& a);
std::ostream& operator<<(std::ostream& output, const Datum& d);
std::istream& operator>>(std::istream& input, Datum& d);

void ulozBin(std::ofstream&output, const Osoba& o);
void nactiBin(std::ifstream&input, Osoba o);
void ulozBin(std::ofstream& output, const Adresa& o);
void nactiBin(std::ifstream& input, Adresa& o);
void ulozBin(std::ofstream& output, const Datum& o);
void nactiBin(std::ifstream& input, Datum& o);
#endif // OSOBA_H

