
#include <fstream>

#include "Osoba.h"


void uloz() {
	const int max = 3;
	Datum datum1{ 6,5,1997 };
	Adresa adresa1{ "halkova", "Humpolec",39601 };
	Datum datum2{ 5,6,1998 };
	Adresa adresa2{ "pricna","Pelhrimov", 36902 };
	Datum datum3{ 6,7,1996 };
	Adresa adresa3{ "rovna", "Jihlava", 39505 };

	Osoba osoba[max];
	osoba[0]._jmeno = "Jan";
	osoba[0]._prijmeni = "Novak";
	osoba[0]._trvaleBydliste = adresa1;
	osoba[0]._datumNarozeni = datum1;

	osoba[1]._jmeno = "Karel";
	osoba[1]._prijmeni = "Novak";
	osoba[1]._trvaleBydliste = adresa2;
	osoba[1]._datumNarozeni = datum2;

	osoba[2]._jmeno = "Josef";
	osoba[2]._prijmeni = "Novak";
	osoba[2]._trvaleBydliste = adresa3;
	osoba[2]._datumNarozeni = datum3;

	std::ofstream soubor;
	soubor.open("soubor.txt");

	soubor << max << std::endl;
	soubor << osoba[0];
	soubor << osoba[1];
	soubor << osoba[2];
	soubor.close();

}

void nacti() {
	int velikost = 0;
	std::ifstream input{};
	input.open("soubor.txt");
	input >> velikost;
	Osoba* o = new Osoba[velikost];
	for (int i = 0; i < velikost; i++)
	{
		input >> o[i];
		std::cout << o[i] << std::endl;
	}
	delete[]o;
}

void ulozBin() {
	const int max = 3;
	Datum datum1{ 6,5,1997 };
	Adresa adresa1{ "halkova", "Humpolec",3900 };
	Datum datum2{ 5,6,1998 };
	Adresa adresa2{ "pricna","Pelhrimov", 3600 };
	Datum datum3{ 6,7,1996 };
	Adresa adresa3{ "rovna", "Jihlava", 3950};

	Osoba osoba[max];
	osoba[0]._jmeno = "Jan";
	osoba[0]._prijmeni = "Novak";
	osoba[0]._trvaleBydliste = adresa1;
	osoba[0]._datumNarozeni = datum1;

	osoba[1]._jmeno = "Karel";
	osoba[1]._prijmeni = "Novak";
	osoba[1]._trvaleBydliste = adresa2;
	osoba[1]._datumNarozeni = datum2;

	osoba[2]._jmeno = "Josef";
	osoba[2]._prijmeni = "Novak";
	osoba[2]._trvaleBydliste = adresa3;
	osoba[2]._datumNarozeni = datum3;
	std::ofstream file;
	file.open("data.bin");
	
	int velikost = 3;
	file.write((const char*)&velikost, sizeof(int));
	ulozBin(file, osoba[0]);
	ulozBin(file, osoba[1]);
	ulozBin(file, osoba[2]);
	
	file.close();
}
void nactiBin() {
	int velikost = 0;
	std::ifstream in{};

	in.open("data.bin");

	in.read((char*)&velikost, sizeof(int));
	Osoba* osoby = new Osoba[velikost];
	for (int i = 0; i < velikost; i++)
	{
		nactiBin(in, osoby[i]);
		std::cout << "nactena osoba je: " << osoby[i]<<std::endl;


	}
	delete[]osoby;
}
int main() {
	
	ulozBin();
	nactiBin();
	
	

	return 0;
}