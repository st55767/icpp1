#include "Osoba.h"
#include <fstream>
#include <iostream>
#include <string>

std::ostream& operator<<(std::ostream& output, const Datum& d) {
	output << d._den << " " << d._mesic << " " << d._rok << std::endl;
	return output;
}

std::istream& operator >>(std::istream& input, Datum& d) {
	input >> d._den;
	input >> d._mesic;
	input >> d._rok;
	return input;
}

void ulozBin( std::ofstream&output, const Osoba& o)
{
	int velikost = o._jmeno.size() + 1;
	output.write((const char*)&velikost, sizeof(int));
	output.write(reinterpret_cast<char*>(&velikost), sizeof(int));
	output.write(o._jmeno.c_str(), velikost);

	velikost = o._prijmeni.size() + 1;
	output.write((const char*)&velikost, sizeof(int));
	output.write(reinterpret_cast<char*>(&velikost), sizeof(int));
	output.write(o._prijmeni.c_str(), velikost);
	output << o._trvaleBydliste;
	output << o._datumNarozeni;
}

void nactiBin(std::ifstream& input, Osoba o)
{
	char* pole;
	int velikost = 0;

	input.read((char*) & (velikost), sizeof(int));
	input.read(reinterpret_cast<char*>(&velikost), sizeof(int));
	pole = new char[velikost];
	input.read(pole, velikost);
	o._jmeno.append(pole, velikost);


	input.read((char*) & (velikost), sizeof(int));
	input.read(reinterpret_cast<char*>(&velikost), sizeof(int));
	pole = new char[velikost];
	input.read(pole, velikost);
	o._prijmeni.append(pole, velikost);

	input >> o._trvaleBydliste;
	input >> o._datumNarozeni;
}



void ulozBin(std::ofstream& output, const Adresa& o)
{
	int velikost = o._mesto.size() + 1;

	output.write((const char*)&velikost, sizeof(int));
	output.write(reinterpret_cast<char*>(&velikost), sizeof(int));
	output.write(o._mesto.c_str(), velikost);

	velikost = o._ulice.size() + 1;

	output.write((const char*)&velikost, sizeof(int));
	output.write(reinterpret_cast<char*>(&velikost), sizeof(int));
	output.write(o._mesto.c_str(),velikost);
	output.write((const char*)&o._psc, sizeof(int));
}

void nactiBin(std::ifstream& input, Adresa& o)
{
	char* pole;
	int velikost = 0;

	input.read((char*) & (velikost), sizeof(int));
	input.read(reinterpret_cast<char*>(&velikost), sizeof(int));
	pole = new char[velikost];
	input.read(pole, velikost);
	o._mesto.append(pole, velikost);


	input.read((char*) & (velikost), sizeof(int));
	input.read(reinterpret_cast<char*>(&velikost), sizeof(int));
	pole = new char[velikost];
	input.read(pole, velikost);
	o._ulice.append(pole, velikost);
	input.read((char*) & (o._psc), sizeof(int));
}

void ulozBin(std::ofstream& output, const Datum& o)
{
	output.write((const char*)&o._den, sizeof(int));
	output.write((const char*)&o._mesic, sizeof(int));
	output.write((const char*)&o._rok, sizeof(int));

}

void nactiBin(std::ifstream& input, Datum& o)
{
	input.read((char*)&o._den, sizeof(int));
	input.read((char*)&o._mesic, sizeof(int));
	input.read((char*)&o._rok, sizeof(int));

}

std::ostream& operator<<(std::ostream& output, const Adresa& a) {
	output << a._ulice << " " << a._mesto << " " << a._psc << std::endl;
	return output;
}

std::istream& operator>> (std::istream& input, Adresa& a) {
	input >> a._ulice;
	input >> a._mesto;
	input >> a._psc;
	return input;
}

std::ostream& operator<<(std::ostream& output, const Osoba& o) {
	output << o._jmeno << " " << o._prijmeni << " " << o._trvaleBydliste << " " << o._datumNarozeni << std::endl;
	return output;
}

std::istream& operator>>(std::istream& input, Osoba& o) {
	input >> o._jmeno;
	input >> o._prijmeni;
	input >> o._trvaleBydliste;
	input >> o._datumNarozeni;
	return input;
}