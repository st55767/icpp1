#ifndef ICOMPARABLE_H
#define ICOMPARABLE_H
#include "IObjekt.h"

struct IComparable : IObjekt
{
public:
	virtual int compareTo(IComparable* obj) const=0;
	IComparable(){}
	virtual ~IComparable(){}


};
#endif // !ICOMPARABLE_H

