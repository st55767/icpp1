#include "Cas.h"

Cas::Cas(int aHodiny, int aMinuty, int aSekundy)
{
	_hodiny = aHodiny;
	_minuty = aMinuty;
	_sekundy = aSekundy;
}

Cas::~Cas()
{}

int Cas::compareTo(IComparable* obj) const
{
	Cas* novyCas = dynamic_cast<Cas*>(obj);

	if (_hodiny < novyCas->_hodiny)
		return 1;
	if (_hodiny > novyCas->_hodiny)
		return -1;
	if (_minuty < novyCas->_minuty)
		return 1;
	if (_minuty > novyCas->_minuty)
		return -1;
	if (_sekundy < novyCas->_sekundy)
		return 1;
	if (_sekundy > novyCas->_sekundy)
		return -1;

	return 0; // jsou stejne
}

std::string Cas::toString() const
{
	return std::to_string(_hodiny) + ":" + std::to_string(_minuty) + ":" + std::to_string(_sekundy);
}
