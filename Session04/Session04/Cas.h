#ifndef CAS_H
#define CAS_H

#include "IComparable.h"
struct Cas : IComparable
{
private:
	int _hodiny;
	int _minuty;
	int _sekundy;

public:
	Cas(int aHodiny, int aMinuty, int aSekundy);
	virtual ~Cas();
	int compareTo(IComparable* obj) const override;
	std::string toString()const override;

};

#endif // CAS_H