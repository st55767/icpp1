#ifndef IOBJEKT_H
#define IOBJEKT_H

#include<string>
struct IObjekt
{
public:
	virtual std::string toString() const =0;
	IObjekt() {}
	virtual ~IObjekt() {}

};
#endif // !IOBJEKT_H
