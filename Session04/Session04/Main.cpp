
#include "IComparable.h"
#include "Cas.h"
#include <ctime>
#include <iostream>

#define VELIKOSTPOLE 10

using namespace std;
void SeraditPole(IComparable** pole, int velikostPole) 
{
	for (int i = 0; i < velikostPole; i++)
	{
		for (int j = i+1; j < velikostPole; j++)
		{
			if (pole[j]->compareTo(pole[i]) == 1)
			{
				IComparable* pom = pole[j];
				pole[j] = pole[i];
				pole[i] = pom;
			}

		}

	}

}

int main()
{
	
	Cas** pole = new Cas * [VELIKOSTPOLE];
	srand(time(nullptr));

	for (int i = 0; i < VELIKOSTPOLE; i++)
	{
		pole[i] = new Cas(rand() % 24, rand() % 60, rand() % 60);

	}

	SeraditPole((IComparable**)pole, VELIKOSTPOLE);

	for ( int i = 0; i < VELIKOSTPOLE; i++)
	{
		cout << "Cas:" << pole[i]->toString() << endl;
	}



	delete[] pole;

}