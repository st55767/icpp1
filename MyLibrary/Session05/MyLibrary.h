#ifndef MYLIBRARY_H
#define MYLIBRARY_H

#ifndef MYLIB_EXPORTS
#define API_MYLIB_LIBRARY __declspec(dllexport)
#else
#define API_MYLIB_LIBRARY __declspec(dllimport)
#endif // MYLIB_EXPORTS

class API_MYLIB_LIBRARY MyLibrary {
public:
	void SayHello()const;
	void SayGoodbye()const;
};


#endif // MYLIBRARY_H
