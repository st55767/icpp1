#include <time.h>
#include <iostream>
#include "Osoba.h"
#include "TelefonniSeznam.h"

int main(int argc, char* argv) {
	
	srand(time(nullptr));
	
	Model::TelefonniSeznam* telefonniSeznam = new Model::TelefonniSeznam();
	for (int i = 0; i < 10; i++)
	{
		Entity::Osoba* osoba = new Entity::Osoba("jmeno" + std::to_string(rand() % 20), std::to_string(rand() % 999999999 + 111111111), i);
		telefonniSeznam->pridejOsobu(*osoba);

	}
	
	std::cout << telefonniSeznam->najdiTelefon("jmeno2") << std::endl;
	std::cout << telefonniSeznam->najdiTelefon(5) << std::endl;

	try {
		std::cout << telefonniSeznam->najdiTelefon(99999) << std::endl;

	}
	catch (std::invalid_argument & ex) {
		std::cout << ex.what() << std::endl;

	}
	catch (Model::no_element & ex2) {
		std::cout << ex2.what() << std::endl;
	}
	return 0;
}