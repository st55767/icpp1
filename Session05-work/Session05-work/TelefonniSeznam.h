#ifndef __TELEFONNISEZNAM
#define __TELEFONNISEZNAM
#include "Osoba.h"
#include <string>
#include <stdexcept>
namespace Model {
	struct no_element : public std::exception {
	public:
		no_element(char const* const zprava) throw();
		virtual char const* what() const throw();
	};
	struct PrvekSeznamu {
	public:
		PrvekSeznamu* dalsi;
		Entity::Osoba data;
		PrvekSeznamu(PrvekSeznamu* aDalsi, Entity::Osoba aData);
		~PrvekSeznamu();
	};

	struct TelefonniSeznam {
	private:
		PrvekSeznamu* zacatek;
	public:
		TelefonniSeznam();
		~TelefonniSeznam();
		void pridejOsobu(Entity::Osoba o);
		std::string  najdiTelefon(std::string aJmeno)const;
		std::string najdiTelefon(int aId) const;

};

}


#endif // __TELEFONNISEZNAM

