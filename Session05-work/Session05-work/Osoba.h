#ifndef __OSOBA
#define __OSOBA
#include <string>
namespace Entity {

	struct Osoba
	{
	private:
		int id;
		std::string jmeno;
		std::string telefon;
	public:
		Osoba();
		Osoba(std::string aJmeno, std::string aTelefon, int aId);
		~Osoba();
		std::string getJmeno()const;
		std::string getTelefon()const;
		int getId()const;
		void setJmeno(std::string aJmeno);
		void setTelefon(std::string aTelefon);
		void setId(int aId);
	
	};
}
#endif // __OSOBA
