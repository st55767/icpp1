#include "TelefonniSeznam.h"
#include "Osoba.h"
#include <stdexcept>


Model::PrvekSeznamu::PrvekSeznamu(PrvekSeznamu* aDalsi, Entity::Osoba aData)
{
	dalsi = aDalsi;
	data = aData;
}

Model::PrvekSeznamu::~PrvekSeznamu()
{
}

Model::TelefonniSeznam::TelefonniSeznam()
{
	zacatek = nullptr;
	
}

Model::TelefonniSeznam::~TelefonniSeznam()
{
	while (zacatek!=nullptr)
	{
		PrvekSeznamu* pom = zacatek->dalsi;
		delete zacatek;
		zacatek = pom;

	}
}

void Model::TelefonniSeznam::pridejOsobu(Entity::Osoba o)
{
	PrvekSeznamu* kontakt = new PrvekSeznamu(zacatek, o);
	kontakt->data.setId(o.getId());
	kontakt->data.setJmeno(o.getJmeno());
	kontakt->data.setTelefon(o.getTelefon());
	zacatek = kontakt;
}

std::string Model::TelefonniSeznam::najdiTelefon(std::string aJmeno) const
{
	if (aJmeno.size() == 0)
	{
		throw std::invalid_argument("Jmeno nesmi byt prazdne"); // jmeno nesmi byt pr�zdn� 
	}

	PrvekSeznamu* hledany = zacatek;
	while (hledany!=nullptr)
	{
		if (aJmeno.compare(hledany->data.getJmeno())==0)
		{
			return hledany->data.getTelefon();
		}
		hledany = hledany->dalsi;

	}
	throw Model::no_element("Prvek nebyl nalezen v seznamu"); // prvek nebyl nalezen v seznamu
}

std::string Model::TelefonniSeznam::najdiTelefon(int aId) const
{
	if (aId<0)
	{
		throw std::invalid_argument("Neplatne Id"); // neplatne id
	}
	PrvekSeznamu* hledany = zacatek;
	while (hledany != nullptr)
	{
		if(hledany->data.getId() ==aId )
		{
			return hledany->data.getTelefon();
		}
		hledany = hledany->dalsi;
	}
	throw Model::no_element("Prvek nebyl nalezen v seznamu"); // prvek neni v seznamu
}

Model::no_element::no_element(char const* const zprava) throw()
{
}

char const* Model::no_element::what() const throw()
{
	return "Prvek neni v seznamu";
}
