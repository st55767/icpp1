#include "Osoba.h"

Entity::Osoba::Osoba()
{
}

Entity::Osoba::Osoba(std::string aJmeno, std::string aTelefon, int aId)
{
	jmeno = aJmeno;
	telefon = aTelefon;
	id = aId;
}

Entity::Osoba::~Osoba()
{
}

std::string Entity::Osoba::getJmeno() const
{
	return jmeno;
}

std::string Entity::Osoba::getTelefon() const
{
	return telefon;
}

int Entity::Osoba::getId() const
{
	return id;
}

void Entity::Osoba::setJmeno(std::string aJmeno)
{
	jmeno = aJmeno;
}

void Entity::Osoba::setTelefon(std::string aTelefon)
{
	telefon = aTelefon;
}

void Entity::Osoba::setId(int aId)
{
	id = aId;
}
