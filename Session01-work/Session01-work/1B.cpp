#include <iostream>

typedef struct Trojuhelnik {
	unsigned int a;
	unsigned int b;
	unsigned int c;

}Trojuhelnik;



bool sestrojitelny(Trojuhelnik t) {
	if ((t.a + t.b > t.c&& t.a + t.c > t.b&& t.b + t.c > t.a)) {
		return true;
	}
	else {
		return false;
	}
}


using namespace std;

int main() {

	Trojuhelnik t;
	cout << "zadejte stranu a:" << endl;
	cin >> t.a;
	cout << "zadejte stranu b:" << endl;
	cin >> t.b;
	cout << "zadejte stranu c:" << endl;
	cin >> t.c;
	if (sestrojitelny(t))
	{
		unsigned int o = t.a + t.b + t.c;
		cout << "obvod trojuhelniku je: " << o << endl;
		return 0;

	}

	cout << "trojuhelnik nelze sestrojit" << endl;
	return 0;

}