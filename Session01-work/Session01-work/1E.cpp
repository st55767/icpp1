#include <iostream>

struct Trojuhelnik {
	unsigned int a;
	unsigned int b;
	unsigned int c;
};

bool lzeSestrojit(Trojuhelnik* t) {
	if ((t->a + t->b > t->c&& t->a + t->c > t->b&& t->b + t->c > t->a)) {
		return true;
	}
	else {
		return false;
	}
}


using namespace std;

int main() {

	 int pocetTrojuhelniku = 0;
	cout << "kolik trojuhelniku chcete vytvorit ?" << endl;
	cin >> pocetTrojuhelniku;

	if (pocetTrojuhelniku <= 0) {
		cout << "nelze sestrojit tento pocet trojuhelniku" << endl;
		return 1;
	}

	Trojuhelnik* poleTrojuhelniku = new Trojuhelnik[pocetTrojuhelniku];

	for (; pocetTrojuhelniku; --pocetTrojuhelniku) {
		Trojuhelnik* t = &(poleTrojuhelniku[pocetTrojuhelniku - 1]);
		cout << " zadejte stranu a: " << endl;
		cin >> t->a;
		cout << " zadejte stranu b: " << endl;
		cin >> t->b;
		cout << " zadejte stranu c: " << endl;
		cin >> t->c;

		if (lzeSestrojit(t))
		{
			unsigned int o = t->a + t->b + t->c;
			cout << "obvod trojuhelniku je: " << o << endl;

		}
		else {
			cout << "trojuhelnik nelze sestrojit" << endl;
		}


	}
	
	delete[]poleTrojuhelniku;
	return 0;

}
