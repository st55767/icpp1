#ifndef POTRUBI_H
#define POTRUBI_H
#include <vector>
#include <exception>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>

enum Typ {
	VYCHOD_ZAPAD,
	SEVER_JICH,
	SEVER_JICH_VYCHOD_ZAPAD,
	NIC,
	UZAVER,
	JICH_VYCHOD_ZAPAD
};

struct APotrubniPrvek;
struct IPotrubi {
	virtual ~IPotrubi() { }
	virtual const APotrubniPrvek* DejPrvek(int x, int y) const = 0;
	virtual bool JePotrubiOk() const = 0;
	virtual const int getSize() const = 0;
};

struct APotrubniPrvek {
	virtual ~APotrubniPrvek() { }
	virtual bool JeKorektneZapojen(const IPotrubi* potrubi) const = 0;
	virtual const Typ DejTyp() const = 0;
	int x;
	int y;
};

class Potrubi : public IPotrubi {
private:
	std::vector<std::vector<APotrubniPrvek*>> matice = std::vector<std::vector<APotrubniPrvek*>>();
	int size = 0;
public:
	Potrubi(const int aX);
	~Potrubi();
	void pridejPrvek(const int x, const int y, APotrubniPrvek* prvek);
	const APotrubniPrvek* DejPrvek(int x, int y) const;
	bool JePotrubiOk() const;
	const int getSize() const;
};

std::ifstream& operator >> (std::ifstream& output, Potrubi& a);

class Prvek : public APotrubniPrvek {
private:
	Typ typ;
public:
	Prvek(const int aX, const int aY, Typ aTyp);
	~Prvek();
	bool JeKorektneZapojen(const IPotrubi* potrubi) const;
	const Typ DejTyp() const;
};

#endif // !POTRUBI_H