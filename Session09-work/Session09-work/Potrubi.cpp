#include "potrubi.h"

Prvek::Prvek(const int aX, const int aY, Typ aTyp)
{
	x = aX;
	y = aY;
	typ = aTyp;
}

Prvek::~Prvek()
{
}

bool Prvek::JeKorektneZapojen(const IPotrubi* potrubi) const
{
	if (this->DejTyp() == VYCHOD_ZAPAD) {
		if (this->y == 0 || this->y == potrubi->getSize() - 1)
			return false;
		Typ nalevo = potrubi->DejPrvek(x, y - 1)->DejTyp();
		Typ napravo = potrubi->DejPrvek(x, y + 1)->DejTyp();
		if (nalevo == NIC || napravo == NIC)
			return false;
	}

	if (this->DejTyp() == SEVER_JICH) {
		if (this->x == 0 || this->x == potrubi->getSize() - 1)
			return false;
		Typ nahore = potrubi->DejPrvek(x - 1, y)->DejTyp();
		Typ dole = potrubi->DejPrvek(x + 1, y)->DejTyp();
		if (nahore == NIC || dole == NIC)
			return false;
	}

	if (this->DejTyp() == SEVER_JICH_VYCHOD_ZAPAD) {
		if (this->y == 0 || this->y == potrubi->getSize() - 1 ||
			this->x == 0 || this->x == potrubi->getSize() - 1)
			return false;
		Typ nalevo = potrubi->DejPrvek(x, y - 1)->DejTyp();
		Typ napravo = potrubi->DejPrvek(x, y + 1)->DejTyp();
		Typ nahore = potrubi->DejPrvek(x - 1, y)->DejTyp();
		Typ dole = potrubi->DejPrvek(x + 1, y)->DejTyp();
		if (nalevo == NIC || napravo == NIC || nahore == NIC || dole == NIC)
			return false;
	}

	if (this->DejTyp() == JICH_VYCHOD_ZAPAD) {
		if (this->y == 0 || this->y == potrubi->getSize() - 1 || this->x == potrubi->getSize() - 1)
			return false;
		Typ nalevo = potrubi->DejPrvek(x, y - 1)->DejTyp();
		Typ napravo = potrubi->DejPrvek(x, y + 1)->DejTyp();
		Typ dole = potrubi->DejPrvek(x + 1, y)->DejTyp();
		if (nalevo == NIC || napravo == NIC || dole == NIC)
			return false;
	}

	return true;
}

const Typ Prvek::DejTyp() const
{
	return typ;
}


Potrubi::Potrubi(const int aSize)
{
	size = aSize;
	for (size_t i = 0; i < aSize; i++)
	{
		matice.insert(matice.begin(), std::vector<APotrubniPrvek*>());
	}
}

Potrubi::~Potrubi()
{
}

void Potrubi::pridejPrvek(const int x, const int y, APotrubniPrvek* prvek)
{
	matice.at(x).insert(matice.at(x).end(), prvek);
}

const APotrubniPrvek* Potrubi::DejPrvek(int x, int y) const
{
	if (matice.size() < x || matice.at(x).size() < y || x < 0 || y < 0) {
		throw std::logic_error("Na techto souradnicich neni zadny prvek.");
	}

	return matice.at(x).at(y);
}

bool Potrubi::JePotrubiOk() const
{
	for (int i = 0; i < size; i++)
	{
		std::vector<APotrubniPrvek*> radek = matice.at(i);
		for (int j = 0; j < size; j++)
			if (radek.at(j)->JeKorektneZapojen(this) == false)
				return false;

	}
	return true;
}

const int Potrubi::getSize() const
{
	return size;
}

std::ifstream& operator >> (std::ifstream& input, Potrubi& a)
{
	char c = 0;
	APotrubniPrvek* newPrvek = nullptr;
	int xNext = -1;

	std::string line;

	while (std::getline(input, line)) {
		int i = 0;
		++xNext;
		int yNext = 0;

		for (i = 0; i < line.length(); i++) {
			c = line.at(i);

			switch (c) {
			case '-':
				newPrvek = new Prvek(xNext, yNext, VYCHOD_ZAPAD);
				break;
			case 'I':
				newPrvek = new Prvek(xNext, yNext, SEVER_JICH);
				break;
			case '+':
				newPrvek = new Prvek(xNext, yNext, SEVER_JICH_VYCHOD_ZAPAD);
				break;
			case 'O':
				newPrvek = new Prvek(xNext, yNext, UZAVER);
				break;
			case 'T':
				newPrvek = new Prvek(xNext, yNext, JICH_VYCHOD_ZAPAD);
				break;
			case ' ':
				newPrvek = new Prvek(xNext, yNext, NIC);
				break;
			}
			a.pridejPrvek(xNext, yNext++, newPrvek);
		}

		for (; i < a.getSize(); i++) //Zarovnani matice
		{
			newPrvek = new Prvek(xNext, yNext, NIC);
			a.pridejPrvek(xNext, yNext++, newPrvek);
		}
	}

	return input;
}